var shipWithinDays = function(weights, days) {
    function getDays(capacity) {
        let dayss = 1, total = 0;
        
        for(let n of weights) {
            total += n;
            if(total > capacity) {
                total = n;
                dayss++;
            }
        }
        return dayss;
    }
    
    let start = Math.max(...weights), 
        end = weights.reduce((acc, cur) => acc + cur, 0);
    
    while(start < end) {
        const mid = Math.floor((end+start)/2);
        const dayss = getDays(mid);
        if(dayss > days) start = mid+1;
        else end = mid;
    }
    return end;
};

console.log(shipWithinDays([1,2,3,4,5,6,7,8,9,10], 5))