function removeNode(list, value) {
    var node = list, prev;
    // si el primero hay que eliminar
    if (node && node.value === value) {
        return node.next;
    }
    // iterar mientras no sea el valor buscado
    while (node && node.value !== value) {
        // elemento previo a
        prev = node,
        // me muevo hacia el que si es
        node = node.next;
    }
    // si tenemos el previo y el valor actual es el buscado
    if (prev && node.value === value) {
        // la referencia del anterior es el subsiguiente
        prev.next = node.next;
    }
    return list;
}

var list = { value: 1, next: { value: 2, next: { value: 3, next: { value: 4, next: { value: 5, next: { value: 6, next: { value: 7, next: null } } } } } } };

list = removeNode(list, 5);
console.log(JSON.stringify(list, null, 3))

// list = removeNode(list, 1);
// console.log(list)

// list = removeNode(list, 7);
// console.log(list)