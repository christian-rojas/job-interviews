var isPalindrome = function(s) {
    if(!s.length) return true
    
    const replaced = s.replace(/[^a-z0-9]/gi, '').toLowerCase();
    let reversedString = ''
	for(let i = replaced.length - 1; i >= 0; i--) {
		reversedString += replaced[i];
	}
	return replaced === reversedString;
};

isPalindrome("A man, a plan, a canal: Panama")