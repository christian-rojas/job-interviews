var dailyTemperatures = function(temperatures) {
    let answer = new Array(temperatures.length).fill(0)
    for(let i=0; i<temperatures.length-1; i++){
        for(let j=i+1;j<temperatures.length;j++){
            if(temperatures[i]<temperatures[j]){
                answer[i] = j-i
                break
            }
        }
    }
    console.log(answer);
    return answer
};
//cuanto dias espero para tener mas temperatura

// dailyTemperatures([30,40,50,60]) // [1,1,1,0]
dailyTemperatures([73,74,75,71,69,72,76,73]) // [1,1,4,2,1,1,0,0]