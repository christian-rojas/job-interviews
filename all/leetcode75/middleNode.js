var middleNode = function(head) { // 1, 2, 3, 4, 5
    let slow = head, fast = head
    while(fast !== null && fast.next !== null){
        slow = slow.next // 2, 3 - 4
        fast = fast.next.next // 3, 5
    }
    return slow
};

// si es par devuelve el de al medio
/*
Input: head = [1,2,3,4,5]
Output: [3,4,5]
/*