var minimizedMaximum = function(n, quantities) {
    // get max value = 11
    const maxVal = Math.max(...quantities)
    let low = 1, high = maxVal
    
    function getCount(size) {
        let result = 0
        for (const q of quantities) {
            // size : binary search middle of values
            const outcome = Math.ceil(q / size) // 11 / 6
            // add to res
            result += outcome
        }
        return result
    }
    // find earliest high such that
    // getCount(high) <= n
    while (low < high) {
        const mid = Math.floor((low + high) / 2) // 1+11 / 2 = 6
        const count = getCount(mid)
        // si el resultado es menor a la cantidad
        // set mayor igual middle
        if (count <= n) high = mid
        // sino, el menor es 1+mid
        else low = 1 + mid
    }
    return high
};

console.log(minimizedMaximum(6, [11,6])) // max(2, 3, 3, 3, 3, 3) = 3.
minimizedMaximum(7, [15,10,10]) // max(5, 5, 5, 5, 5, 5, 5) = 5.