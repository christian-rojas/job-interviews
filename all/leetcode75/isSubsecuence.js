var isSubsequence = function(s, t) {
    let cont = 0
    for(let i=0; i<t.length; i++){
        if(t[i] === s[cont]){
            cont++
        }
    }
    return cont === s.length
};

isSubsequence(s = "abc", t = "ahbgdc")