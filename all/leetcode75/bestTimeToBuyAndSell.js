const maxProfit = (prices) => {
    let left = 0; // Buy
    let right = 1; // sell
    let max_profit = 0;
    while (right < prices.length) {
      if (prices[left] < prices[right]) {
        let profit = prices[right] - prices[left]; // our current profit
        max_profit = Math.max(max_profit, profit);
      } else {
        left = right;
      }
      right++;
    }
    console.log(max_profit);
    return max_profit;
};

maxProfit([7,1,5,3,6,4]) // 6-1 = 5

  /**
    var min = Number.MAX_SAFE_INTEGER; 
    var max = 0;
    for (var i = 0; i < prices.length; i++) {
        min = Math.min(min, prices[i]);
        max = Math.max(max, prices[i]-min);
    }
    return max;
   */