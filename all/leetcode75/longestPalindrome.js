var longestPalindrome = function(s) {
    let ans = 0;
    let keys = {};
    for (let char of s) {
      // cuenta las coincidencias
      keys[char] = (keys[char] || 0) + 1;
      // si es par agrego 2
      if (keys[char] % 2 == 0) ans += 2;
    }
    // si el string es mayor a la respuesta, retorno la respuesta mas 1 y sino la respuesta
    return s.length > ans ? ans + 1 : ans;
};

longestPalindrome(s = "abccccdd") // One longest palindrome that can be built is "dccaccd", whose length is 7.