function ListNode(val, next) {
    this.val = (val===undefined ? 0 : val)
    this.next = (next===undefined ? null : next)
}

var mergeTwoLists = function(l1, l2) {
    let list = new ListNode()
    let head = list
    while (l1 !== null && l2 !== null) {
		// Select the smallest value from either linked list,
		// then increment that list forward.
        if (l1.val < l2.val) {
            // el siguiente es 
            //list.next = new ListNode(l1.val) es mas rapido
            list.next = l1
            // l1++
            l1 = l1.next
        } else {
            //list.next = new ListNode(l2.val)
            list.next = l2
            //l2++
            l2 = l2.next
        }
        // i++
        list = list.next
    }
	// It's possible that one linked list is shorter than the other so we just
	// add on the remainder of the last linked list. It's already sorted :)
    if (l1 !== null)
        list.next = l1
    if (l2 !== null)
        list.next = l2
	// return .next because this first element in the linkedlist is empty
    return head.next
};

console.info(mergeTwoLists([1,2,4], [1,3,4])) // [1,1,2,3,4,4]