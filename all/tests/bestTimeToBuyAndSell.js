function buyandsell(nums) {
    let left=0
    let right=1
    let result=0
    while(right<nums.length){
        if(nums[left]<nums[right]){
            const profit = nums[right] - nums[left]
            result = Math.max(profit, result)
        }else{
            left=right
        }
        right++
    }
    console.log(result);
}

buyandsell([7,1,3,4,6,5])
// space = O(n)
// time = O(n)
