var containsDuplicate = function(nums) {
    let map = new Map()
    for(const item of nums){
        if(map.has(item)) return true
        map.set(item, map.get(item) + 1)
    }
    return false
};

containsDuplicate([1,2,3,1])