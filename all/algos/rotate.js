/**
 * @param {number[]} nums
 * @param {number} k
 * @return {void} Do not return anything, modify nums in-place instead.
 */
 var rotate = function(nums, k) {
    while (k--) {
        nums.unshift( nums.pop() );
    }
    console.log(nums);
};

rotate([1,2,3,4,5,6,7], 2)

//[1,2,3,4,5,6,7]

/**
 * 
var rotate = function(nums, k) {
  k=k%nums.length
  reverse(0,nums.length-1,nums)
  reverse(0,k-1,nums)
  reverse(k,nums.length-1,nums)
}
let reverse=(l,r,nums)=>{
     while(l<r){
     [nums[l],nums[r]]=[nums[r],nums[l]] //using ES7 Destructuring Assignment Array Matching to perform a swap
     l++
     r--
 }
}
 */