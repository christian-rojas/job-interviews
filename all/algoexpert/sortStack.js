function sortStack(stack) {
    if(stack.length === 0) return stack;
  
    const top = stack.pop()
  
    sortStack(stack)
  
    insertInSortOrder(stack, top)
  
    return stack
  }
  
  function insertInSortOrder(stack, value) {
    if(stack.length === 0 || stack[stack.length - 1] <= value) {
      stack.push(value)
      return
    }
    const top = stack.pop()
  
    insertInSortOrder(stack, value)
  
    stack.push(top)
  }

console.log(sortStack([-5, 2, -2, 4, 3, 1]));