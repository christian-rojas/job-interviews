function sunsetViews(buildings, direction) {
	const buildingsWithSunset = [];
	const startIdx = direction === 'WEST' ? 0 : buildings.length - 1;
	const step = direction === 'WEST' ? 1 : -1;

	let idx = startIdx;
	let runningMaxHeight = 0;
	while (idx >= 0 && idx < buildings.length) {
		const buildingHeight = buildings[idx];
		if (buildingHeight > runningMaxHeight) buildingsWithSunset.push(idx);
		runningMaxHeight = Math.max(runningMaxHeight, buildingHeight);
		idx = idx + step;
	}

	if (direction === 'EAST') buildingsWithSunset.reverse();

	return buildingsWithSunset;
}

const buildings = [3, 5, 4, 4, 3, 1, 3, 2]
const direction = 'EAST'

console.log(sunsetViews(buildings, direction));
