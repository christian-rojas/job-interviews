var maxProfit = function(prices) {
    let left=0, right=1
    let result=0
    while(right < prices.length){
        if(prices[left]<prices[right]){
            let profit = prices[right] - prices[left]
            result += profit
            left=right
        }else{
             left++
        }
        right++
    }
    return result
};

maxProfit([7,1,5,3,6,4]) // Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
// Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.
// Total profit is 4 + 3 = 7.