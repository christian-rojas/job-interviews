var singleNumber = function(nums) {
    const uniques = new Map()
    for(const num of nums){
        uniques[num] = uniques[num] + 1 || 1
    }
    for(const key in uniques){
        if(uniques[key] === 1){
            return key
        }
    }
    
};

singleNumber([4,1,2,1,2]) // 4
// time: O(n)
// space: O(n) hash table