var isValidSudoku = function (board) {
    // iterate the whole array
	for (let i = 0; i < 9; i++) {
		let row = new Set(),
			col = new Set(),
			box = new Set();
        // iterate each box
		for (let j = 0; j < 9; j++) {
			let _row = board[i][j];
			let _col = board[j][i];
			// console.log(i, j);
			let _box = board[3 * Math.floor(i / 3) + Math.floor(j / 3)][3 * (i % 3) + (j % 3)];
			
			// [3 * 0/3 + 0/3][3 * ]
			// 3 * 0/3 * 1/3
			// 3 * 0 % 3 + 0 % 3 = 0
			// 3 * 0%3 + 1%3 = 1
			// 3* 0 % 3 + 3 % 3 = 1

			//board row 3 y col 3
			//box: board[3*1+1][3*0+0] = board[4][0]

			if (_row != '.') {
                // si ya existe el numero, retorna false
				if (row.has(_row)) return false;
                // sino, agrega el numero
				row.add(_row);
			}
			if (_col != '.') {
				if (col.has(_col)) return false;
				col.add(_col);
			}
			
			if(i == 3 && j == 6) {
				console.log(3 * Math.floor(i / 3) + Math.floor(j / 3), 3 * (i % 3) + (j % 3));
				console.log(_box);
			}
			if (_box != '.') {
				if (box.has(_box)) return false;
				box.add(_box);
			}
		}
	}
	return true;
};

const valid =   [["5","3",".",".","7",".",".",".","."]
				,["6",".",".","1","9","5",".",".","."]
				,[".","9","8",".",".",".",".","6","."]
				,["8",".",".",".","6",".",".",".","3"]
				,["4",".",".","8",".","3",".",".","1"]
				,["7",".",".",".","2",".",".",".","6"]
				,[".","6",".",".",".",".","2","8","."]
				,[".",".",".","4","1","9",".",".","5"]
				,[".",".",".",".","8",".",".","7","9"]]

const noValid = [["8","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]

// no valid beacuse there two 8s

console.log(isValidSudoku(valid));

