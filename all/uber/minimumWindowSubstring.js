function minimumWindow(s1, s2) {
    let map = new Map()
    for(let i=0; i < s1.length; i++){
        if(s2.includes(s1[i])) {
            map.set(s1[i], i)
        }
    }
    const arr = Array.from(map.entries()).sort((a,b) => a[1] - b[1]);
    let str = ''
    for(let i=arr[0][1]; i<=arr[arr.length-1][1]; i++){
        str+=s1[i]
    }
    return str
}

const s = "ADOBECODEBANC", t = "ABC"

minimumWindow(s, t)