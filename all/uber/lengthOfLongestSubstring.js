var lengthOfLongestSubstringTwoDistinct = function(s) {
    // max number of repeating character = 2
    let max = 0
    let left = 0, right = 0
    let map = new Map()
    while(right < s.length) {
        map.set(s[right], right)
        // si es 3 empiezo a eliminar el de menor indice (que esta mas atras)
        if(map.size === k+1) {
            let minIndex = Math.min(...map.values())
            map.delete(s[minIndex])
            // el de mas izquierda de los 2 que quedan
            left = minIndex + 1
        }
        // en la primera no alcanza entrar al if
        max = Math.max(max, right - left + 1) // la formula es : el mas a la derecha (limite) - el mas a la izquierda
        // console.log('max: ', max); // en la tercera iteracion queda el 3
        right++
    }
    return max
    
};

lengthOfLongestSubstringTwoDistinct("eceba") // 3
// lengthOfLongestSubstringTwoDistinct("ccaabbb") // 5