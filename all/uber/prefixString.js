function prefixString(array1, array2) {
    const str = array1.join('')
    for(const item of array2) {
        if(str.slice(0, item.length) !== item) return false
    }
    return true
}

console.log(prefixString(["one", "two", "three"], ["onetwo", "one"]))