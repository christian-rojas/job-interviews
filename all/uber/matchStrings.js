function breakingBad(name, symbols){
    let correctName = ''
    for(const currName of name.split(" ")){
        correctName += currName[0].toUpperCase() + currName.slice(1, currName.length)+" "
    }
    let arr = [] 
    let currNameNow = ''
    correctName.split(" ").pop()
    for(const currName of correctName.split(" ")){
        const currArr = []
        for(const sym of symbols){
            if(currName.slice(0, sym.length) == sym){
                currArr.push(sym)
            }
        }
        let max = -Infinity
        let c = ''
        for(const item of currArr){
            max = Math.max(max, item.length)
            if(item.length >= max){
                c = item
            }
        }
        if(c !== ''){
            currNameNow+=`[${c}]`+currName.slice(c.length, currName.length)+" "
        }
    }
    console.log(currNameNow);

}

const symbols = ["He","H","Al","B"]

breakingBad("henry alba", symbols)