var rotate = function(matrix) {
    // total rows
    let n = matrix.length // 3
    // profundidad
    let depth = (n / 2) // 1
    for (let i = 0; i < depth; i++) {
        let len = n - 2 * i - 1 // 3-2*0-1 = 2
        let opp = n - 1 - i // 3-1-0 = 2
        for (let j = 0; j < len; j++) {
            let temp = matrix[i][i+j]
            matrix[i][i+j] = matrix[opp-j][i]
            matrix[opp-j][i] = matrix[opp][opp-j]
            matrix[opp][opp-j] = matrix[i+j][opp]
            matrix[i+j][opp] = temp
        }
    }
    return matrix
};

console.log(rotate([[1,2,3],[4,5,6],[7,8,9]])) // roatet 90 grades