function solution(numbers, left, right) {
    for(let i=0; i<numbers.length; i++){
        for(let j=left; j<=right; j++){
            if(numbers[i] == (i+1)*j){
               numbers[i] = true
            }
        }
        numbers[i] = numbers[i] !== true ? false : true
    }
    return numbers
}

