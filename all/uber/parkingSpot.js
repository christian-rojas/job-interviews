/**
 * https://codefights.com/fight/bcGYdzggbiGGWfQ9d
 * This time you are an Uber driver and you are trying your best to park your car in a parking lot.

Your car has length carDimensions[0] and width carDimensions[1]. You have already picked your lucky spot (rectangle of the same size as the car with upper left corner at (luckySpot[0], luckySpot[1])) and bottom right corner at (luckySpot[2], luckySpot[3]) and you need to find out if it's possible to park there or not.

It is possible to park your car at a given spot if and only if you can drive through the parking lot without any turns to your lucky spot (for safety reasons, the car can only move in two directions inside the parking lot - forwards or backwards along the long side of the car) starting from some side of the lot (all four sides are valid options).

Naturally, you can't park your car if the lucky spot is already occupied. The car can't drive through or park at any of the occupied spots.
 */

function solution(carDimensions, parkingLot, luckySpot) {

    const t = luckySpot[0], l = luckySpot[1], b = luckySpot[2], r = luckySpot[3];
    const iend = parkingLot.length - 1, jend = parkingLot[0].length - 1;
    const w = r - l + 1, h = b - t + 1;
    // check si el lucky spot esta ocupado
    checkSpot = checkBlock(parkingLot, t, b, l, r);
    // si el ancho es mayor al alto del auto
    if (w >= h) {
        // primero checkea si existen elementos vacios desde la izquierda
        // luego de la derecha
        return checkSpot && (checkBlock(parkingLot, t, b, 0, l - 1) || checkBlock(parkingLot, t, b, r + 1, jend));
    } else {
        // checkea si hay elementosde arriba a abjo
        // luego de abajo a arriba
        return checkSpot && (checkBlock(parkingLot, 0, t - 1, l, r) || checkBlock(parkingLot, b + 1, iend, l, r));
    }
}

function checkBlock(parkingLot, i1, i2, j1, j2) {
    for (let i = i1; i <= i2; i++) {
        for (let j = j1; j <= j2; j++) {
            if (parkingLot[i][j] > 0) return false;
        }
    }
    return true;
}

solution([3,2], [[1, 0, 1, 0, 1, 0], [0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 1], [1, 0, 1, 1, 1, 1]], [1, 1, 2, 3])