const minMeetingRooms = function(intervals) {
    const starts = intervals.map(x => x[0]);
    const ends = intervals.map(x => x[1]);
    
    starts.sort((a, b) => a - b);
    ends.sort((a, b) => a - b);
    
    let roomsNeeded = 0, e = 0;
    
    for(let s = 0; s < starts.length; s++) {
        // si el comienzo es mayor o igual al termino, no intersectan
        if(starts[s] >= ends[e]) {
            // no necesito mas rooms
            roomsNeeded--;
            // avanzo los termino
            e++;
        }
        // siempre aumento las rooms
        roomsNeeded++;
    }
    return roomsNeeded
};

// minMeetingRooms([[0,30],[5,10],[15,20]]) // 2

// minMeetingRooms([[7,10],[2,4]]) // 1

// minMeetingRooms([[1,5],[8,9],[8,9]]) // 2

minMeetingRooms([[9,10],[4,9],[4,17]]) // 2
// [4,9] - [4,17] - [9,10]