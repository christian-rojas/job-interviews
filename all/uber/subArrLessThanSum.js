function lessThanSum(arr, suma) {
	let start = 0, end = 0;
	let count = 0, sum = arr[0];

	while (start < arr.length && end < arr.length) {
		// If sum is less than k,
		// move end by one position.
		// Update count and sum
		// accordingly.
		if (sum < suma) {
			end++;
			if (end >= start) count += end - start;
			// For last element,
			// end may become n.
			if (end < arr.length) sum += arr[end];
		}
		// If sum is greater than or
		// equal to k, subtract
		// arr[start] from sum and
		// decrease sliding window by
		// moving start by one position
		else {
			sum -= arr[start];
			start++;
		}
	}
    console.log(count);
	return count;
}

lessThanSum([1, 11, 2, 3, 15], 10);
