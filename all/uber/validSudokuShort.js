function validSudoku(sudoku) {
    const rows = [], cols = [], boxes = []
    for(let i = 0; i < sudoku.length; i++){
        rows.push(new Set())
        cols.push(new Set())
        boxes.push(new Set())
    }
    for(let i=0; i<sudoku.length; i++){
        for(let j=0; j<sudoku[i].length; j++){
            const cell = sudoku[i][j]
            if(cell === '.') continue
            const boxNum = 3 * Math.floor(i/3) + Math.floor(j/3) // number of the box
            if(rows[i].has(cell) || cols[j].has(cell) || boxes[boxNum].has(cell)) return false
            rows[i].add(cell)
            cols[j].add(cell)
            boxes[boxNum].add(cell) // asigna el valor a la caja correspondiente
        }
    }
    return true
}

const valid =   [["5","3",".",".","7",".",".",".","."]
				,["6",".",".","1","9","5",".",".","."]
				,[".","9","8",".",".",".",".","6","."]
				,["8",".",".",".","6",".",".",".","3"]
				,["4",".",".","8",".","3",".",".","1"]
				,["7",".",".",".","2",".",".",".","6"]
				,[".","6",".",".",".",".","2","8","."]
				,[".",".",".","4","1","9",".",".","5"]
				,[".",".",".",".","8",".",".","7","9"]]

console.log(validSudoku(valid))