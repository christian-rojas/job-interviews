function longestSubstring(string, k){
    const map = {}
    const n = string.length
    let i = 0;
    let j = 0;
    let result = 0;
    let maxCount = 0;
    while(j<n){
        // si ya existe, aumento
        if(map[string[j]]){
            map[string[j]]++
        // sino, seteo en 1
        }else{
            map[string[j]] = 1
        }
        // obtengo la maxima actual cuenta
        maxCount = Math.max(maxCount, map[string[j]])
        // mientras sea mayor al numero de descuento
        while((j-i+1) - maxCount > k){
            // eliminamos el actual
            map[string[i]]--;
            i++
        }
        // el maximo entre el anterior o el actual
        result = Math.max(result, j-i+1)
        j++
    }
    console.log(result);
    return result
}

// reeamplazar k veces para obtener el maximo substring consecutivo
longestSubstring("AABABBA", 1)