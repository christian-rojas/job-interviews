function solution(departure, destination) {
    let sum=0
    if(destination[0] > departure[0]){
        if(Math.floor(departure[0]) === Math.floor(destination[0])){
            sum+=Math.ceil(departure[0]) - departure[0]
            sum+=Math.ceil(destination[0]) - destination[0]
        }else{
            sum+=destination[0] - departure[0]
        }
        if(destination[1] > departure[1]) {
            if(destination[1] === 0.5 || Math.floor(destination[1]) === Math.floor(departure[1])){
                sum+=destination[1] + departure[1]
            }else{
                sum+=destination[1] - departure[1]
            }
        }else{
            if(destination[1] === 0.5){
                sum+=destination[1] + departure[1]
            }else{
                sum+=departure[1] - destination[1]
            }
        } 
    }
    return sum
}

/*
departure: [0.4, 1]
destination: [0.9, 3]
0.6 + 2 + 0.1 = 2.7

departure: [0, 0.4]
destination: [1, 0.6]
=2

departure: [0.9, 6]
destination: [1.1, 5]
=1.2

departure: [2.4, 1]
destination: [5, 7.3]
=8.9
*/