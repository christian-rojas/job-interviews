const longestSubstring = function(s, k) {
    let map = new Map()
    for(const letter of s){
        if(map.get(letter)){
            map.set(letter, map.get(letter) + 1)
        }else{
            map.set(letter, 1)
        }
    }
    let res = ''
    for(let item of map.entries()) {
        if(item[1]>=2){
            res+=item[0].repeat(item[1])
        }else{
            console.log(res.length);
            return res.length
        }
    }
    console.log(res.length);

    return res.length
};

longestSubstring("ababbc", 2)
// longestSubstring("aaabb", 3)