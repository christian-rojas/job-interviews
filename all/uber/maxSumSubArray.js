function maxSumSubArray(array) {
    const n = array.length;
    let res = -Infinity;
    let curMax = -Infinity;
  
    for (let j = 0; j < n; j ++)
    {
        // check si resta o no
        curMax = Math.max(array[j], curMax + array[j]);
        // si es negativo antes, entonces esto queda igual
        // verifica si el anterior es mayor o no
        res = Math.max(res, curMax);
    }
    console.log(res);
    return res;
}

// maxSumSubArray([-2,1,-3,4,-1,2,1,-5,4]) // [4,-1,2,1] has the largest sum = 6.
maxSumSubArray([-1])

