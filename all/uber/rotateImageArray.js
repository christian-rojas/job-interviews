function rotate(array) {
    const N = array[0].length
    for(let i=0; i< Math.floor(N/2); i++) {
        for(let j=0; j<(N/2); j++){
            const temp = array[N-j-1][i]
            array[N-j-1][i] = array[N-i-1][N-j-1]
            array[N-i-1][N-j-1] = array[j][N-i-1]
            array[j][N-i-1] = array[i][j]
            array[i][j] = temp
        }
    }
    console.table(array)
    return array
}

rotate([[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14,15],[16,17,18,19,20],[21,22,23,24,25]])

// rotate([[1,2,3],[4,5,6],[7,8,9]])